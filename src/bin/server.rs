use std::io::Read;
use std::io::Write;
use std::net::TcpListener;
use std::str;
use std::thread::sleep;
use std::time::Duration;

fn main() {
    let listener = TcpListener::bind("localhost:789").unwrap();
    let mut buff = [0; 4];
    println!("Ping server running...");
    loop {
        match listener.accept() {

            Ok((mut socket, addr)) => {
                println!("Connected to {}", addr);
                while let Ok(_) = socket.read_exact(&mut buff) {
                    println!("Received {}", str::from_utf8(&buff).unwrap());
                    sleep(Duration::from_millis(500));
                    socket.write_all(b"Pong").unwrap();
                    println!("Sent Pong");
                }
            }
            Err(e) => {
                println!("Failed to connect to client: {:?}", e);
            }
        }
    }
}
