use std::io::Read;
use std::io::Write;
use std::net::TcpStream;
use std::str::from_utf8;
use std::thread::sleep;
use std::time::Duration;

fn main() {
    let mut buff = [0; 4];
    if let Ok(mut stream) = TcpStream::connect("localhost:789") {
        println!("Connected to server...");
        loop {
            stream.write_all(b"Ping").unwrap();
            println!("Sent Ping");
            stream.read_exact(&mut buff).unwrap();
            println!("Received {}", from_utf8(&buff).unwrap());
            sleep(Duration::from_millis(500));
        }
    }
    eprintln!("Unable to connect to server");
}
